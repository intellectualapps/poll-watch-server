/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.manager;

import com.pw.app.data.manager.ExceptionThrowerManagerLocal;
import com.pw.app.data.manager.PartyScoreDataManagerLocal;
import com.pw.app.pojo.AppBoolean;
import com.pw.app.util.CodeGenerator;
import com.pw.app.util.Verifier;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.pw.app.util.exception.GeneralAppException;
import io.jsonwebtoken.Claims;
import java.util.ArrayList;
import java.util.logging.Logger;
import com.pw.app.model.PartyScore;
import com.pw.app.util.DateUtil;
import com.pw.app.manager.PartyScoreManagerLocal;

/**
 *
 * @author buls
 */
@Stateless
public class PartyScoreManager implements PartyScoreManagerLocal {

    @EJB
    private PartyScoreDataManagerLocal partyScoreDataManager;
    
    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;

    @EJB
    private DeviceTokenManagerLocal deviceTokenManager;
    
    @EJB
    private CodeGenerator codeGenerator;
    
    @EJB
    private Verifier verifier;
    
    private final String PROJECT_LINK = "/project";
    private final long TOKEN_LIFETIME= 31556952000l;
    private final long PUBLISH_TIMEOUT = 1000;
    
    private Logger logger = Logger.getLogger(PartyScoreManager.class.getName());
        
    @Override
    public AppBoolean savePartyScores(String projectId, String partyScores, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(PROJECT_LINK).verifyParams(projectId, partyScores);
        
        Claims claims = verifier.setResourceUrl(PROJECT_LINK).verifyJwt(rawToken);
        String agentIdInToken = claims.getId();
        logger.info("agentid in token: " + agentIdInToken);
        
        AppBoolean isSaved = new AppBoolean();
        isSaved.setStatus(Boolean.FALSE);
        List<PartyScore> allPartyScores = createPartyScores(projectId, agentIdInToken, partyScores);
        if (!allPartyScores.isEmpty()) {
            isSaved.setStatus(Boolean.TRUE);
        }
        
        return isSaved;
    }

    
    private List<PartyScore> createPartyScores(String projectId, String agentId, String partyScores) {
        List<PartyScore> allPartyScores = new ArrayList<PartyScore>();
        String[] parties = partyScores.split(",");
        for (int i = 0; i < parties.length; i++) {
            String[] partyAndScore = parties[i].split("-");
            String partyId = partyAndScore[0];
            String score = partyAndScore[1];
            PartyScore partyScore = new PartyScore();            
            partyScore.setPartyId(Integer.parseInt(partyId));
            partyScore.setProjectId(projectId);
            partyScore.setAgentId(agentId);
            partyScore.setScore(Integer.parseInt(score));
            partyScore.setDateCreated(DateUtil.getNow());
            partyScore.setDateModified(DateUtil.getNow());
            
            partyScore = partyScoreDataManager.create(partyScore);
            
            allPartyScores.add(partyScore);
        }
        return allPartyScores;
    }
}
