/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.manager;

import com.pw.app.data.manager.DataAccessDataManagerLocal;
import com.pw.app.data.manager.ExceptionThrowerManagerLocal;
import com.pw.app.data.manager.ProjectDataManagerLocal;
import com.pw.app.model.DeviceToken;
import com.pw.app.model.User;
import com.pw.app.pojo.AppBoolean;
import com.pw.app.pojo.AppUser;
import com.pw.app.pojo.UserPayload;
import com.pw.app.util.CodeGenerator;
import com.pw.app.util.JWT;
import com.pw.app.util.JikaDecode;
import com.pw.app.util.MD5;
import com.pw.app.util.SocialPlatformType;
import com.pw.app.util.Verifier;
import java.util.Calendar;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.pw.app.util.exception.GeneralAppException;
import io.jsonwebtoken.Claims;
import java.util.ArrayList;
import java.util.logging.Logger;
import com.pw.app.data.manager.UserDataManagerLocal;
import com.pw.app.model.DataAccess;
import com.pw.app.model.Project;
import com.pw.app.pojo.AppDataAccess;
import com.pw.app.util.DateUtil;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author buls
 */
@Stateless
public class ProjectManager implements ProjectManagerLocal {

    @EJB
    private ProjectDataManagerLocal projectDataManager;
    
    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;

    @EJB
    private DeviceTokenManagerLocal deviceTokenManager;
    
    @EJB
    private CodeGenerator codeGenerator;
    
    @EJB
    private Verifier verifier;
    
    private final String PROJECT_LINK = "/project";
    private final long TOKEN_LIFETIME= 31556952000l;
    private final long PUBLISH_TIMEOUT = 1000;
    
    private Logger logger = Logger.getLogger(ProjectManager.class.getName());
     
    @Override
    public AppBoolean saveProject(String projectId, String agentId, String puId, 
            String electionId, String latitude, String longitude, String altitude, 
            String accuracy, String ec8aUrl, String registeredVoters, String accreditedVoters, 
            String validVotes, String rejectedVotes, String deviceId, String simSerial, 
            String metaInstanceId, String nullificationReason, String rawToken)  throws GeneralAppException {
        
        verifier.setResourceUrl(PROJECT_LINK).verifyParams(projectId, agentId, registeredVoters,
                accreditedVoters, validVotes, rejectedVotes);
        
        Claims claims = verifier.setResourceUrl(PROJECT_LINK).verifyJwt(rawToken);
        String agentIdInToken = claims.getId();
        logger.info("agentid in token: " + agentIdInToken);
        logger.info("PU-ID: " + puId);
        
        Project project = new Project();
        project.setId(projectId);
        project.setAgentId(agentId);
        project.setPuId(puId != null && !puId.isEmpty() ? Integer.parseInt(puId) : 0);
        project.setElectionId(electionId != null && !electionId.isEmpty() ? Integer.parseInt(electionId) : -1);  
        project.setLatitude(latitude != null && !latitude.isEmpty() ? Double.parseDouble(latitude) : 0.0);  
        project.setLongitude(longitude != null && !longitude.isEmpty() ? Double.parseDouble(longitude) : 0.0);  
        project.setAltitude(altitude != null && !altitude.isEmpty() ? Double.parseDouble(altitude) : 0.0);  
        project.setAccuracy(accuracy != null && !accuracy.isEmpty() ? Double.parseDouble(accuracy) : 0.0);  
        project.setEc8aUrl(ec8aUrl);  
        project.setRegisteredVoters(Integer.parseInt(registeredVoters));  
        project.setAccreditedVoters(Integer.parseInt(accreditedVoters));  
        project.setValidVotes(Integer.parseInt(validVotes));  
        project.setRejectedVotes(Integer.parseInt(rejectedVotes)); 
        project.setTotalVotesCast(Integer.parseInt(validVotes) + Integer.parseInt(rejectedVotes)); 
        project.setDeviceId(deviceId); 
        project.setSimSerial(simSerial); 
        project.setMetaInstanceId(metaInstanceId);
        project.setDateCreated(DateUtil.getNow());
        project.setStartDate(DateUtil.getNow());
        project.setEndDate(DateUtil.getNow());
        project.setSubmissionDate(DateUtil.getNow());
        project.setDateModified(DateUtil.getNow());
        project.setNullificationReason(nullificationReason);
        
        project = projectDataManager.create(project);
        
        AppBoolean appBoolean = new AppBoolean();
        appBoolean.setStatus(Boolean.TRUE);
        
        return appBoolean;
    }
    
}
