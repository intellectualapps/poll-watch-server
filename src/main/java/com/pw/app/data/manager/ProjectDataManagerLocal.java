/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.data.manager;

import com.pw.app.model.Project;
import java.util.List;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

/**
 *
 * @author buls
 */
@Local
public interface ProjectDataManagerLocal {
    
    Project create(Project project) throws EJBTransactionRolledbackException;

    Project update(Project project);

    Project get(String projectId);

    void delete(Project project);
    
    List<Project> getByUsername(String username);
}
