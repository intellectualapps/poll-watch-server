/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.data.manager;

import com.pw.app.data.provider.DataProviderLocal;
import com.pw.app.model.DataAccess;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

/**
 *
 * @author buls
 */
@Stateless
public class DataAccessDataManager implements DataAccessDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public DataAccess create(DataAccess dataAccess) throws EJBTransactionRolledbackException {
        return crud.create(dataAccess);
    }

    @Override
    public DataAccess update(DataAccess dataAccess) {
        return crud.update(dataAccess);
    }

    @Override
    public DataAccess get(String dataAccessId) {
        return crud.find(dataAccessId, DataAccess.class);
    }

    @Override
    public void delete(DataAccess dataAccess) {
        crud.delete(dataAccess);
    }
  
    @Override
    public List<DataAccess> getByUsername(String username) {        
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("username", username);
        return crud.findByNamedQuery("DataAccess.findByUsername", parameters, DataAccess.class);
    }    
}

