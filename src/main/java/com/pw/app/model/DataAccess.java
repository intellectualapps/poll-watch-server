/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author buls
 */

@Entity
@Table(name = "data_access")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "DataAccess.findByUsername", query = "SELECT d FROM DataAccess d WHERE d.username = :username")})

public class DataAccess implements Serializable {
    @Id
    @NotNull    
    @Column(name = "id")
    private String id;
    
    @Column(name = "agent_id")
    private String agentId;
    
    @Column(name = "username")
    private String username;    
        
    @Column(name = "passwd")
    private String password;
    
    @Column(name = "puid")
    private Integer puId;
    
    @Column(name = "election_id")
    private Integer electionId;
    
    @Column(name = "election_type")
    private String electionType;
    
    @Column(name = "election_date")
    private String electionDate;
    
    @Column(name = "delimitation")
    private String delimiter;
        
    public DataAccess() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }
   
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }    
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPuId() {
        return puId;
    }

    public void setPuId(Integer puId) {
        this.puId = puId;
    }

    public Integer getElectionId() {
        return electionId;
    }

    public void setElectionId(Integer electionId) {
        this.electionId = electionId;
    }

    public String getElectionType() {
        return electionType;
    }

    public void setElectionType(String electionType) {
        this.electionType = electionType;
    }

    public String getElectionDate() {
        return electionDate;
    }

    public void setElectionDate(String electionDate) {
        this.electionDate = electionDate;
    }

    public String getDelimiter() {
        return delimiter;
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }
    
    
            
}
