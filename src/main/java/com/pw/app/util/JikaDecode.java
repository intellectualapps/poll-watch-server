/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.pw.app.util;

import java.io.Serializable;
import java.util.*;
/**
 *
 * @author Buls
 */
public class JikaDecode implements Serializable{

    public Vector decode(String encoded){
        Vector decodedString = new Vector();
        int i;
        int j;
        int njj =0;
        String[] ls;
        String [] enc = encoded.split("\\s");

        for(i = 0; i < enc.length; i++){        // for each token
            int letters = enc[i].length()/2;
            ls = new String[letters];
            for(j = 0; j < enc[i].length(); j++){   //for each letter in the token


                int nj = j+2;
                try{

                ls[njj] = enc[i].substring(j,nj);
                } catch(Exception e){ 
                    System.out.println("*** incomplete message\n\nthe encoded "
                            + "message u entered is incorrect, you must have missed"
                            + " a number please check the encoded message and try again"+e);
                    //System.exit(0);
                }
                j++;

                String dcd = reDecode(ls[njj]);
                njj++;
                decodedString.add(dcd.toLowerCase());
            }

           njj = 0;
        }

        
        return decodedString;
    }

    public String decode(String encoded, int returnType){
        Vector decodedString = new Vector();
        int i;
        int j;
        int njj =0;
        String[] ls;
        String [] enc = encoded.split("\\s");

        for(i = 0; i < enc.length; i++){        // for each token
            int letters = enc[i].length()/2;
            ls = new String[letters];
            for(j = 0; j < enc[i].length(); j++){   //for each letter in the token


                int nj = j+2;
                try{

                ls[njj] = enc[i].substring(j,nj);
                } catch(Exception e){ 
                    System.out.println("*** incomplete message\n\nthe encoded "
                            + "message u entered is incorrect, you must have missed "
                            + "a number please check the encoded message and try again"+e);                    
                }
                j++;

                String dcd = reDecode(ls[njj]);
                njj++;
                decodedString.add(dcd.toLowerCase());
            }

           njj = 0;
        }

        Enumeration e = decodedString.elements();
        String fullyDecoded = "";
        while(e.hasMoreElements()){
        	fullyDecoded = fullyDecoded.concat((String)e.nextElement());
        }
        
        return fullyDecoded;
    }


    public static void showHeader(){
        System.out.println("");
        System.out.println("************************************");
        System.out.println("Jika-Decoder Version 0.1.011008");
        System.out.println("Developed by: Buls");
        System.out.println("Release Date: 01/10/2008");
        System.out.println("************************************");
        System.out.println("\n\n");
    }


    public String reDecode(String slEnc){
        String dcd = "("+slEnc.toString()+")";

        if(slEnc.equals("11")){
            dcd = "A";
            return dcd;
        } else if(slEnc.equals("21")){
            dcd = "B";
            return dcd;
        } else if(slEnc.equals("31")){
            dcd = "C";
            return dcd;
        } else if(slEnc.equals("41")){
            dcd = "D";
            return dcd;
        } else if(slEnc.equals("51")){
            dcd = "E";
            return dcd;
        } else if(slEnc.equals("61")){
            dcd = "F";
            return dcd;
        } else if(slEnc.equals("71")){
            dcd = "G";
            return dcd;
        } else if(slEnc.equals("12")){
            dcd = "H";
            return dcd;
        } else if(slEnc.equals("22")){
            dcd = "I";
            return dcd;
        } else if(slEnc.equals("32")){
            dcd = "J";
            return dcd;
        } else if(slEnc.equals("42")){
            dcd = "K";
            return dcd;
        } else if(slEnc.equals("52")){
            dcd = "L";
            return dcd;
        } else if(slEnc.equals("62")){
            dcd = "M";
            return dcd;
        } else if(slEnc.equals("72")){
            dcd = "N";
            return dcd;
        } else if(slEnc.equals("13")){
            dcd = "O";
            return dcd;
        } else if(slEnc.equals("23")){
            dcd = "P";
            return dcd;
        } else if(slEnc.equals("33")){
            dcd = "Q";
            return dcd;
        } else if(slEnc.equals("43")){
            dcd = "R";
            return dcd;
        } else if(slEnc.equals("53")){
            dcd = "S";
            return dcd;
        } else if(slEnc.equals("63")){
            dcd = "T";
            return dcd;
        } else if(slEnc.equals("73")){
            dcd = "U";
            return dcd;
        } else if(slEnc.equals("14")){
            dcd = "V";
            return dcd;
        } else if(slEnc.equals("24")){
            dcd = "W";
            return dcd;
        } else if(slEnc.equals("34")){
            dcd = "X";
            return dcd;
        } else if(slEnc.equals("44")){
            dcd = "Y";
            return dcd;
        } else if(slEnc.equals("54")){
            dcd = "Z";
            return dcd;
        } else if(slEnc.equals("64")){
            dcd = "1";
            return dcd;
        } else if(slEnc.equals("74")){
            dcd = "2";
            return dcd;
        } else if(slEnc.equals("15")){
            dcd = "3";
            return dcd;
        } else if(slEnc.equals("25")){
            dcd = "4";
            return dcd;
        } else if(slEnc.equals("35")){
            dcd = "5";
            return dcd;
        } else if(slEnc.equals("45")){
            dcd = "6";
            return dcd;
        } else if(slEnc.equals("55")){
            dcd = "7";
            return dcd;
        } else if(slEnc.equals("65")){
            dcd = "8";
            return dcd;
        } else if(slEnc.equals("75")){
            dcd = "9";
            return dcd;
        } else if(slEnc.equals("16")){
            dcd = "0";
            return dcd;
        } else if(slEnc.equals("26")){
            dcd = "`";
            return dcd;
        } else if(slEnc.equals("36")){
            dcd = "¬";
            return dcd;
        } else if(slEnc.equals("46")){
            dcd = "!";
            return dcd;
        } else if(slEnc.equals("56")){
            dcd = "\"";
            return dcd;
        } else if(slEnc.equals("66")){
            dcd = "£";
            return dcd;
        } else if(slEnc.equals("76")){
            dcd = "$";
            return dcd;
        } else if(slEnc.equals("17")){
            dcd = "%";
            return dcd;
        } else if(slEnc.equals("27")){
            dcd = "^";
            return dcd;
        } else if(slEnc.equals("37")){
            dcd = "&";
            return dcd;
        } else if(slEnc.equals("47")){
            dcd = "*";
            return dcd;
        } else if(slEnc.equals("57")){
            dcd = "(";
            return dcd;
        } else if(slEnc.equals("67")){
            dcd = ")";
            return dcd;
        } else if(slEnc.equals("77")){
            dcd = "_";
            return dcd;
        } else if(slEnc.equals("18")){
            dcd = "-";
            return dcd;
        } else if(slEnc.equals("28")){
            dcd = "+";
            return dcd;
        } else if(slEnc.equals("38")){
            dcd = "=";
            return dcd;
        } else if(slEnc.equals("48")){
            dcd = "@";
            return dcd;
        } else if(slEnc.equals("58")){
            dcd = "'";
            return dcd;
        } else if(slEnc.equals("68")){
            dcd = "#";
            return dcd;
        } else if(slEnc.equals("78")){
            dcd = "~";
            return dcd;
        } else if(slEnc.equals("19")){
            dcd = ";";
            return dcd;
        } else if(slEnc.equals("29")){
            dcd = ":";
            return dcd;
        } else if(slEnc.equals("39")){
            dcd = "?";
            return dcd;
        } else if(slEnc.equals("49")){
            dcd = "/";
            return dcd;
        } else if(slEnc.equals("59")){
            dcd = ">";
            return dcd;
        } else if(slEnc.equals("69")){
            dcd = ".";
            return dcd;
        } else if(slEnc.equals("79")){
            dcd = "<";
            return dcd;
        } else if(slEnc.equals("10")){
            dcd = ",";
            return dcd;
        } else if(slEnc.equals("20")){
            dcd = "\\";
            return dcd;
        } else if(slEnc.equals("30")){
            dcd = "|";
            return dcd;
        } else if(slEnc.equals("40")){
            dcd = " ";
            return dcd;
        }
        return dcd;
    }


}
