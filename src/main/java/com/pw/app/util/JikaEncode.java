/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.pw.app.util;

import java.io.Serializable;

/**
 *
 * @author Buls
 */
public class JikaEncode implements Serializable {      

    public String encode(String encode){
        int i;
        int j;
        int njj = 0;
        String[] ls;
        String [] enc = encode.toUpperCase().split("\\s");
        String oneWord = "";

        for(i = 0; i < enc.length; i++){        // for each token
            int letters = enc[i].length();
            if(letters == 0){
                ls = new String[2];
            } else {
                ls = new String[letters];
            }
            for(j = 0; j < enc[i].length(); j++){   //for each letter in the token


                int nj = j+1;

                ls[njj] = enc[i].substring(j,nj);                

                String dcd = reencode(ls[njj]);
                njj++;
                oneWord = oneWord.concat(dcd);                
            }
            njj = 0;
        }
        
        return oneWord;
    }

    public static void showHeader(){
        System.out.println("");
        System.out.println("************************************");
        System.out.println("Jika-Encoder Version 0.1.011008");
        System.out.println("Developed by: Buls");
        System.out.println("Release Date: 01/10/2008");
        System.out.println("************************************");
        System.out.println("\n\n");
    }



    public String reencode(String slEnc){
        String dcd = null;

        if(slEnc.equals("A")){
            dcd = "11";
            return dcd;
        } else if(slEnc.equals("B")){
            dcd = "21";
            return dcd;
        } else if(slEnc.equals("C")){
            dcd = "31";
            return dcd;
        } else if(slEnc.equals("D")){
            dcd = "41";
            return dcd;
        } else if(slEnc.equals("E")){
            dcd = "51";
            return dcd;
        } else if(slEnc.equals("F")){
            dcd = "61";
            return dcd;
        } else if(slEnc.equals("G")){
            dcd = "71";
            return dcd;
        } else if(slEnc.equals("H")){
            dcd = "12";
            return dcd;
        } else if(slEnc.equals("I")){
            dcd = "22";
            return dcd;
        } else if(slEnc.equals("J")){
            dcd = "32";
            return dcd;
        } else if(slEnc.equals("K")){
            dcd = "42";
            return dcd;
        } else if(slEnc.equals("L")){
            dcd = "52";
            return dcd;
        } else if(slEnc.equals("M")){
            dcd = "62";
            return dcd;
        } else if(slEnc.equals("N")){
            dcd = "72";
            return dcd;
        } else if(slEnc.equals("O")){
            dcd = "13";
            return dcd;
        } else if(slEnc.equals("P")){
            dcd = "23";
            return dcd;
        } else if(slEnc.equals("Q")){
            dcd = "33";
            return dcd;
        } else if(slEnc.equals("R")){
            dcd = "43";
            return dcd;
        } else if(slEnc.equals("S")){
            dcd = "53";
            return dcd;
        } else if(slEnc.equals("T")){
            dcd = "63";
            return dcd;
        } else if(slEnc.equals("U")){
            dcd = "73";
            return dcd;
        } else if(slEnc.equals("V")){
            dcd = "14";
            return dcd;
        } else if(slEnc.equals("W")){
            dcd = "24";
            return dcd;
        } else if(slEnc.equals("X")){
            dcd = "34";
            return dcd;
        } else if(slEnc.equals("Y")){
            dcd = "44";
            return dcd;
        } else if(slEnc.equals("Z")){
            dcd = "54";
            return dcd;
        } else if(slEnc.equals("1")){
            dcd = "64";
            return dcd;
        } else if(slEnc.equals("2")){
            dcd = "74";
            return dcd;
        } else if(slEnc.equals("3")){
            dcd = "15";
            return dcd;
        } else if(slEnc.equals("4")){
            dcd = "25";
            return dcd;
        } else if(slEnc.equals("5")){
            dcd = "35";
            return dcd;
        } else if(slEnc.equals("6")){
            dcd = "45";
            return dcd;
        } else if(slEnc.equals("7")){
            dcd = "55";
            return dcd;
        } else if(slEnc.equals("8")){
            dcd = "65";
            return dcd;
        } else if(slEnc.equals("9")){
            dcd = "75";
            return dcd;
        } else if(slEnc.equals("0")){
            dcd = "16";
            return dcd;
        } else if(slEnc.equals("`")){
            dcd = "26";
            return dcd;
        } else if(slEnc.equals("¬")){
            dcd = "36";
            return dcd;
        } else if(slEnc.equals("!")){
            dcd = "46";
            return dcd;
        } else if(slEnc.equals("\"")){
            dcd = "56";
            return dcd;
        } else if(slEnc.equals("£")){
            dcd = "66";
            return dcd;
        } else if(slEnc.equals("$")){
            dcd = "76";
            return dcd;
        } else if(slEnc.equals("%")){
            dcd = "17";
            return dcd;
        } else if(slEnc.equals("^")){
            dcd = "27";
            return dcd;
        } else if(slEnc.equals("&")){
            dcd = "37";
            return dcd;
        } else if(slEnc.equals("*")){
            dcd = "47";
            return dcd;
        } else if(slEnc.equals("(")){
            dcd = "57";
            return dcd;
        } else if(slEnc.equals(")")){
            dcd = "67";
            return dcd;
        } else if(slEnc.equals("_")){
            dcd = "77";
            return dcd;
        } else if(slEnc.equals("-")){
            dcd = "18";
            return dcd;
        } else if(slEnc.equals("+")){
            dcd = "28";
            return dcd;
        } else if(slEnc.equals("=")){
            dcd = "38";
            return dcd;
        } else if(slEnc.equals("@")){
            dcd = "48";
            return dcd;
        } else if(slEnc.equals("'")){
            dcd = "58";
            return dcd;
        } else if(slEnc.equals("#")){
            dcd = "68";
            return dcd;
        } else if(slEnc.equals("~")){
            dcd = "78";
            return dcd;
        } else if(slEnc.equals(";")){
            dcd = "19";
            return dcd;
        } else if(slEnc.equals(":")){
            dcd = "29";
            return dcd;
        } else if(slEnc.equals("?")){
            dcd = "39";
            return dcd;
        } else if(slEnc.equals("/")){
            dcd = "49";
            return dcd;
        } else if(slEnc.equals(">")){
            dcd = "59";
            return dcd;
        } else if(slEnc.equals(".")){
            dcd = "69";
            return dcd;
        } else if(slEnc.equals("<")){
            dcd = "79";
            return dcd;
        } else if(slEnc.equals(",")){
            dcd = "10";
            return dcd;
        } else if(slEnc.equals("\\")){
            dcd = "20";
            return dcd;
        } else if(slEnc.equals("|")){
            dcd = "30";
            return dcd;
        } else if(slEnc.equals(" ")){
            dcd = "40";
            return dcd;
        }

        return dcd;


    }

}
