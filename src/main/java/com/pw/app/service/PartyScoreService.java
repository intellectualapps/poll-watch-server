/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.app.service;


import com.pw.app.manager.PartyScoreManagerLocal;
import com.pw.app.manager.ProjectManagerLocal;
import com.pw.app.manager.UserManagerLocal;
import com.pw.app.model.DeviceToken;
import com.pw.app.pojo.AppBoolean;
import com.pw.app.pojo.AppUser;
import com.pw.app.pojo.UserDataAccessPayload;
import com.pw.app.pojo.UserPayload;
import com.pw.app.util.exception.GeneralAppException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author buls
 */
@Stateless
@Path("/v1/party-scores")
public class PartyScoreService {
    
    @Context
    HttpServletRequest request;   
    
    @EJB    
    PartyScoreManagerLocal partyScoreManager;
                  
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveProject(@QueryParam("project-id") String projectId,
            @QueryParam("party-scores") String partyScores,
            @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        AppBoolean appBoolean = partyScoreManager.savePartyScores(projectId, partyScores, rawToken);
        return Response.ok(appBoolean).build();
           
    }
        
}
